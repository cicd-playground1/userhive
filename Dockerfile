# Base image
FROM python:3.9-slim-buster

# Set the working directory
WORKDIR /userhive

# Copy the requirements file into the container
COPY requirements.txt requirements.txt

# Install the requirements
RUN pip3 install -r requirements.txt

# Copy the Python script into the container
COPY userhive-python/ /userhive/
