import sys
import os
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

import pytest
import requests_mock

from app.user_fetcher import fetch_all_users

def test_fetch_all_users_empty_list_on_failure():
    with requests_mock.Mocker() as mock:
        mock.get('https://reqres.in/api/users', status_code=404)

        users = fetch_all_users()

        assert users == []


def test_fetch_all_users_error_raised_on_non_json_response():
    with requests_mock.Mocker() as mock:
        mock.get('https://reqres.in/api/users', text='Not a JSON response')

        with pytest.raises(ValueError):
            fetch_all_users()

def test_fetch_all_users_data():
    all_users = fetch_all_users()

    assert isinstance(all_users, list)
    assert all_users != []
