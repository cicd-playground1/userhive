import sys
import os
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

import pytest
from unittest.mock import mock_open, patch

from app.export_html import export_to_html

def test_export_to_html_writes_expected_html():
    users = [
        {"avatar": "avatar1.jpg", "first_name": "John", "last_name": "Doe", "email": "johndoe@example.com"},
        {"avatar": "avatar2.jpg", "first_name": "Jane", "last_name": "Doe", "email": "janedoe@example.com"}
    ]

    # Define the expected HTML output for the given user list
    expected_html = (
        '<!DOCTYPE html>\n<html>\n<head>\n<link rel="stylesheet" href="list.css">\n</head>\n<body>\n\n'
        '<div class="content">\n\n\t<ul class="team">\n'
        '\t\t<li class="member">\n\t\t\t<div class="thumb"><img src="avatar1.jpg"></div>\n'
        '\t\t\t<div class="description">\n\t\t\t\t<h3>John Doe</h3>\n'
        '\t\t\t\t<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna helloworld<br>'
        '<a href="avatar1.jpg">johndoe@example.com</a></p>\n\t\t\t</div>\n\t\t</li>\n'
        '\t\t<li class="member">\n\t\t\t<div class="thumb"><img src="avatar2.jpg"></div>\n'
        '\t\t\t<div class="description">\n\t\t\t\t<h3>Jane Doe</h3>\n'
        '\t\t\t\t<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna helloworld<br>'
        '<a href="avatar2.jpg">janedoe@example.com</a></p>\n\t\t\t</div>\n\t\t</li>\n'
        '\t</ul>\n\n</div>\n\n</body>\n</html>'
    )
    
    # Patch the open() function so that we can test what's written to the file
    with patch("builtins.open", mock_open()) as mock_file:
        # Call the function being tested
        export_to_html(users)

        # Assert that the expected HTML was written to the file
        mock_file().write.assert_called_once_with(expected_html)
