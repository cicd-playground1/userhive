import os
from typing import List
import shutil


def export_to_html(users: List[dict]):
    """
    Exports the list of users to an HTML file and displays their images.
    """
    print(f"DEBUG [export_to_html]: Starting ...")

    if not os.path.exists("/userhive/app/html"):
        os.makedirs("/userhive/app/html")

    with open('/userhive/app/html/users.html', 'w') as f:
        html = '<!DOCTYPE html>\n<html>\n<head>\n<link rel="stylesheet" href="list.css">\n</head>\n<body>\n\n<div class="content">\n\n\t<ul class="team">\n'
        loremipsum = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna helloworld'

        for user in users:
            html += (f'\t\t<li class="member">\n\t\t\t<div class="thumb"><img src="{user["avatar"]}"></div>\n\t\t\t<div class="description">\n\t\t\t\t<h3>{user["first_name"]} {user["last_name"]}</h3>\n\t\t\t\t'
                     f'<p>{loremipsum}<br><a href="{user["avatar"]}">{user["email"]}</a></p>\n\t\t\t</div>\n\t\t</li>\n')

        html += '\t</ul>\n\n</div>\n\n</body>\n</html>'

        f.write(html)
        print(f"DEBUG [export_to_html]: HTML file '/userhive/app/html/users.html' successfully generated")


def copy_to_mounted_output_folder():
    shutil.copytree("/userhive/app/html", "/userhive/output", dirs_exist_ok=True)
    print(f"DEBUG [copy_to_mounted_output_folder]: HTML files copied to /userhive/output")
