from typing import List
import requests

def fetch_all_users() -> List[dict]:
    """
    Fetches all users from the ReqRes API and returns them as a list of dictionaries.
    """
    base_url = 'https://reqres.in/api/users'
    all_users = []

    # Fetch the first page of users to determine the total number of pages
    response = requests.get(base_url)

    if response.status_code != 200:
        return all_users
    
    data = response.json()
    total_pages = data['total_pages']

    print(f"DEBUG [fetch]: Total pages = {total_pages}")


    # Fetch all pages of users and append them to the all_users list
    for page_number in range(1, total_pages + 1):
        response = requests.get(base_url, params={'page': page_number})
        data = response.json()
        users = data['data']
        all_users += users

    return all_users
