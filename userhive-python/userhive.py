from app.user_fetcher import fetch_all_users
from app.export_html import export_to_html, copy_to_mounted_output_folder

if __name__ == "__main__":
    print("INFO: userhive.py started ...")

    all_users = fetch_all_users()
    export_to_html(all_users)
    copy_to_mounted_output_folder()
