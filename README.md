# UserHive

_This project is part of a "Coding Challenge"_

UserHive is a Python application that fetches all users from `https://reqres.in/api/users` and exports them as HTML file.

## Releases

https://gitlab.com/cicd-playground1/userhive/-/releases

## Prerequisites

Before proceeding, make sure you have the following installed:

* Docker (version 19 or later)

### Pull the latest version
`docker pull paetzold/userhive:latest`

### Building the Docker image

`docker build -t userhive:0.1 .`

### Running the Docker container

`mkdir output`\
`docker run -v $(pwd)/output:/userhive/output/ paetzold/userhive:latest python3 /userhive/userhive.py`

## CI/CD highlights

* Every change to the Python app is built and tested after pushing the commit
    * After `build:userhive`, browsing the job artifacts leads to a preview of the HTML
    * After `test:userhive`, a pytest unit test report is available in the logs

* After a MR has been merged to the default branch, other kind of jobs will run.
    * Update of docker image will be pushed to DockerHub.
    * GitLab release will be created.
    * A webhook triggers the "deploy-to-s3" pipeline is the project `userhive-html-s3`

## Potential improvements

* Use of docker-compose
* Show results of unit tests in GitLab MR
* Install own GitLab runner on EC2 using Terraform

## Authentication

`DOCKERHUB_USERNAME` and `DOCKERHUB_PASSWORD` are stored as GitLab CI/CD variables

## Difficulties

### Accessing container registry

Issue: No Group/Project Access token for trial user
https://forum.gitlab.com/t/i-cannot-find-group-access-token-permission-under-general-advanced-permissions/80050/9

Resolution: Push it to DockerHub instead